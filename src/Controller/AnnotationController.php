<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class AnnotationController
{
  /**
   * @Route("/hello")
   */
    public function hello(): Response
    {
        return new Response(
            '<html><body>Hello You !</body></html>'
        );
    }
    /**
     * @Route("/hello1")
     */
      public function hello1(): Response
      {
          return new Response(
              '<html><body>Hello You !</body></html>'
          );
      }
}
