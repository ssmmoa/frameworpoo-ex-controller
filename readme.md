# Création de controlleurs

Les controlleurs reçoivent les requettes http du client
Les réponses qu'il transmettent sont ce qui sera retourné au client

Dans cet exemple vous pouvez voir des réponses en JSON (avec l'objet Response et l'objet JsonResponse), en HTML (objet Response)

Ceux-ci sont visibles dans le répertoire ou se trouvent les Controller (src/controller)


## Routage
Il y a deux manières de gérer le routage (= redirection à partir d'un URL vers une fonction d'un controlleur) en symfony:

- via la configuration (méthode peu utilisée voir désuette)

    configuration de la route: controllers/config/routes.yaml
    controlleur : Controller\RouteController::number  
    (fonction number du controlleur RouteController)

- via les annotations (méthode priviligiée)

    ex: AnnotationController
    annotation :
    ```
    /**
     * @Route("/hello")
     */
     ```
     utilisation requise de l'objet Route: use Symfony\Component\Routing\Annotation\Route;
